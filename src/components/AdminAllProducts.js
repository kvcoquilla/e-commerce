import {useState, useEffect, useContext, Fragment} from 'react'
import {Container} from 'react-bootstrap'
//import {Navigate, useNavigate, Link} from 'react-router-dom'
//import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import AdminProdCard from '../components/AdminProdCard'

export default function AdminAllProducts(){

	const [adminproduct, setAdminProduct] = useState([])
	
	useEffect(() => {
		fetch(`https://warm-hamlet-58634.herokuapp.com/products/all`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setAdminProduct(data.map(adminproduct => {
				
				return(
					<Fragment>
					
					<AdminProdCard key={adminproduct.id} adminProdProp = {adminproduct}/>
								
					</Fragment>
				)
			}))
		})
	}, [])


	return(
		<Fragment>
		<Container>
			<h1 className="my-4">All Products</h1>
			<div>
			{adminproduct}
			</div>
	
			</Container>
		</Fragment>
	)
	
}